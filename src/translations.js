const TRANSLATIONS = {
  DE: {
    'upload.successfulImported': 'E-Mail erfolgreich an Tocco übermittelt!',
    'upload.successfulExisting': 'E-Mail existiert bereits im Tocco!',
    'upload.missingRole':
      'Sie haben nicht die notwendigen Benutzerrechte. Es wird die Login-Rolle Outlook Add-in benötigt.',
    'upload.error': 'Beim Übermitteln ans Tocco ist ein Fehler aufgetreten.',
    'upload.errorAttachment': 'Es ist ein Fehler beim Hochladen vom Anhang aufgetreten!',
    'configuration.description':
      'Wenn Sie das Tocco-Outlook Add-in verwenden möchten, benötigen Sie zuerst einen API-Key. Einmal eingerichtet, können Sie mit dem Tocco-Outlook Add-in Ihre E-Mails bequem ans Tocco übermitteln.',
    'configuration.documentation': 'Anleitung im Handbuch für das Einrichten vom Outlook Add-In',
    'configuration.moduleNotInstalled':
      'Die Lizenz "E-Mail-Anbindung Microsoft 365 Outlook" wurde noch nicht aktiviert. Bitte wenden Sie sich an Ihren Tocco-Administrator.',
    'configuration.invalidSettings': 'Die Kombination aus Tocco-Instanz (URL), Benutzername und API-Key ist ungültig.',
    'configuration.missingRole':
      'Sie haben nicht die notwendigen Benutzerrechte. Es wird die Login-Rolle Outlook Add-in benötigt.',
    'configuration.url': 'Tocco-Instanz (URL)',
    'configuration.username': 'Benutzername',
    'configuration.apiKey': 'API-Key',
    'configuration.save': 'Speichern'
  },
  EN: {
    'upload.successfulImported': 'E-Mail successfully sent to Tocco!',
    'upload.successfulExisting': 'E-Mail already exists in Tocco!',
    'upload.missingRole': 'You do not have the required access rights. The login role Outlook add-in is required.',
    'upload.error': 'An error occurred during the transmission to Tocco.',
    'upload.errorAttachment': 'An error occurred while uploading the attachment!',
    'configuration.description':
      'If you wish to use the Tocco Outlook add-in, you will need an API key first. Once set up, the Tocco Outlook add-in allows you to effortlessly submit your e-mails to the Tocco system.',
    'configuration.documentation': 'Instructions in the manual on how to set up the Outlook add-in',
    'configuration.moduleNotInstalled':
      'The license "E-mail integration Microsoft 365 Outlook" has not been activated yet. Please contact your Tocco administrator.',
    'configuration.invalidSettings': 'The combination of Tocco instance (URL), user name and API key is invalid.',
    'configuration.missingRole':
      'You do not have the required access rights. The login role "Outlook add-in” is required.',
    'configuration.url': 'Tocco instance (URL)',
    'configuration.username': 'Username',
    'configuration.apiKey': 'API key',
    'configuration.save': 'Save'
  },
  FR: {
    'upload.successfulImported': 'E-mail transféré avec succès à Tocco !',
    'upload.successfulExisting': "L'e-mail existe déjà chez Tocco !",
    'upload.missingRole':
      "Vous ne disposez pas des droits d'utilisation nécessaires. Le rôle de connexion Module complémentaire Outlook est nécessaire.",
    'upload.error': 'Une erreur est survenue lors de la transmission à Tocco.',
    'upload.errorAttachment': "Une erreur est survenue lors du téléversement de l'annexe !",
    'configuration.description':
      "Si vous souhaitez utiliser le module complémentaire Outlook de Tocco, vous aurez d’abord besoin d'une clé API. Une fois l’installation terminée, vous pouvez confortablement transférer vos e-mails à Tocco à l’aide du module complémentaire Outlook de Tocco.",
    'configuration.documentation': "Consignes dans le manuel d'installation du module complémentaire Outlook",
    'configuration.moduleNotInstalled':
      "La licence « Liaison d'e-mails Microsoft 365 Outlook » n'a pas encore été activée. Veuillez vous adresser à votre administrateur Tocco.",
    'configuration.invalidSettings':
      "La combinaison de l'instance Tocco (URL), du nom d'utilisateur et de la clé API est invalide.",
    'configuration.missingRole':
      "Vous ne disposez pas des droits d'utilisation nécessaires. Le rôle de connexion « Module complémentaire Outlook » est nécessaire.",
    'configuration.url': 'Instance Tocco (URL)',
    'configuration.username': "Nom d'utilisateur",
    'configuration.apiKey': 'Clé API',
    'configuration.save': 'Sauvegarder'
  },
  IT: {
    'upload.successfulImported': 'E-mail correttamente trasmessa a Tocco!',
    'upload.successfulExisting': 'E-mail già esistente in Tocco!',
    'upload.missingRole':
      'Non dispone dei diritti utente necessari. È necessario il ruolo di login Componente aggiuntivo di Outlook.',
    'upload.error': 'Nella trasmissione a Tocco si è verificato un errore.',
    'upload.errorAttachment': 'Si è verificato un errore durante il caricamento dell’allegato!',
    'configuration.description':
      'Se desidera usare il componente aggiuntivo di Outlook Tocco, prima di tutto serve una chiave API. Una volta impostato, con il componente aggiuntivo di Outlook Tocco può trasmettere comodamente le Sue e-mail a Tocco.',
    'configuration.documentation': 'Istruzioni nel manuale di configurazione del componente aggiuntivo di Outlook',
    'configuration.moduleNotInstalled':
      'La licenza "Integrazione e-mail Microsoft 365 Outlook" non è stata ancora attivata. Si rivolga al suo amministratore di Tocco.',
    'configuration.invalidSettings':
      'La combinazione di istanza di Tocco (URL), nome utente e chiave API non è valida.',
    'configuration.missingRole':
      'Non dispone dei diritti utente necessari. È necessario il ruolo di login "Componente aggiuntivo di Outlook".',
    'configuration.url': 'Istanza di Tocco (URL)',
    'configuration.username': 'Nome utente',
    'configuration.apiKey': 'API key',
    'configuration.save': 'Salva'
  }
}

const getTranslation = key => {
  const locale = Office.context.displayLanguage
  let localizedTranslations = TRANSLATIONS.DE

  if (locale.startsWith('en-')) {
    localizedTranslations = TRANSLATIONS.EN
  } else if (locale.startsWith('fr-')) {
    localizedTranslations = TRANSLATIONS.FR
  } else if (locale.startsWith('it-')) {
    localizedTranslations = TRANSLATIONS.IT
  }

  return localizedTranslations[key]
}
