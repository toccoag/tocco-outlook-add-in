Office.onReady(async info => {
  if (info.host !== Office.HostType.Outlook) {
    return
  }

  const setElementText = (id, translationKey) => {
    document.getElementById(id).innerHTML = getTranslation(translationKey)
  }

  setElementText('description', 'configuration.description')
  setElementText('documentation', 'configuration.documentation')
  setElementText('urlLabel', 'configuration.url')
  setElementText('usernameLabel', 'configuration.username')
  setElementText('apiKeyLabel', 'configuration.apiKey')
  setElementText('saveSettings', 'configuration.save')

  document.getElementById('saveSettings').onclick = saveSettings

  const {url: savedUrl, username: savedUsername} = getToccoSettings()

  if (savedUrl) {
    document.getElementById('url').value = savedUrl
  }
  if (savedUsername) {
    document.getElementById('username').value = savedUsername
  }
})

const saveSettings = async () => {
  const url = document.getElementById('url').value
  const username = document.getElementById('username').value
  const apiKey = document.getElementById('apiKey').value

  const errorMessageEl = document.getElementById('errorMessage')
  const saveButton = document.getElementById('saveSettings')

  errorMessageEl.style.display = 'none'
  saveButton.disabled = true

  const showError = translationKey => {
    errorMessageEl.innerHTML = getTranslation(translationKey)
    errorMessageEl.style.display = 'block'
    saveButton.disabled = false
  }

  if (!(await isModuleInstalled(url))) {
    return showError('configuration.moduleNotInstalled')
  }

  if (!(await areValidSettings(url, username, apiKey))) {
    return showError('configuration.invalidSettings')
  }

  if (!(await hasOutlookAddinRole(url, username, apiKey))) {
    return showError('configuration.missingRole')
  }

  Office.context.ui.messageParent(JSON.stringify({type: 'save', url, username, apiKey}))
}
