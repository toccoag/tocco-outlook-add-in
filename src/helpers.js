const URL_KEY = 'tocco.url'
const USERNAME_KEY = 'tocco.username'
const APIKEY_KEY = 'tocco.apiKey'

const getToccoSettings = () => {
  const settings = Office.context.roamingSettings
  return {
    url: settings.get(URL_KEY),
    username: settings.get(USERNAME_KEY),
    apiKey: settings.get(APIKEY_KEY)
  }
}

const saveToccoSettings = (url, username, apiKey, callback = () => {}) => {
  const settings = Office.context.roamingSettings
  settings.set(URL_KEY, url)
  settings.set(USERNAME_KEY, username)
  settings.set(APIKEY_KEY, apiKey)
  settings.saveAsync(callback)
}

const isModuleInstalled = async url => {
  try {
    const response = await fetch(`${url}/nice2/rest/modules`)
    const json = await response.json()
    return json.modules.includes('nice.optional.officeintegration')
  } catch (e) {
    console.log(e)
    return false
  }
}

const areValidSettings = async (url, username, apiKey) => {
  try {
    const response = await fetch(`${url}/nice2/username`, {
      headers: {
        Authorization: 'Basic ' + btoa(username + ':' + apiKey)
      }
    })
    const json = await response.json()
    return json.username === username
  } catch (e) {
    console.log(e)
    return false
  }
}

const hasOutlookAddinRole = async (url, username, apiKey) => {
  try {
    const response = await fetch(`${url}/nice2/rest/officeintegration/hasOutlookAddInLoginRole`, {
      headers: {
        Authorization: 'Basic ' + btoa(username + ':' + apiKey)
      }
    })
    const json = await response.json()
    return json.hasLoginRole
  } catch (e) {
    console.log(e)
    return false
  }
}

const extractEmailAddress = emailAddressDetails => emailAddressDetails?.emailAddress

const showNotification = message => {
  const details = {
    type: Office.MailboxEnums.ItemNotificationMessageType.InformationalMessage,
    message,
    icon: 'icon1',
    persistent: false
  }
  Office.context.mailbox.item.notificationMessages.replaceAsync('import', details, () => {})
}

// returns the uuid of the Temp_upload
// if an error occured an empty string is returned
const uploadAttachment = async (base64content, filename) => {
  try {
    const mimeType = getMimeTypeFromExtension(filename)
    const blob = base64ToBlob(base64content, mimeType)

    const formData = new FormData()
    formData.append('file', blob)

    const {url, username, apiKey} = getToccoSettings()
    const response = await fetch(`${url}/nice2/upload`, {
      method: 'POST',
      headers: {
        Authorization: 'Basic ' + btoa(username + ':' + apiKey)
      },
      body: formData
    })

    if (response.status === 200) {
      const json = await response.json()
      return json.id
    }
  } catch (e) {
    console.log(e)
  }
  return ''
}

const base64ToBlob = (base64String, mimeType) => {
  const base64Data = base64String.replace(/^data:.+;base64,/, '')
  const byteCharacters = atob(base64Data)
  const byteNumbers = new Array(byteCharacters.length)

  for (let i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i)
  }

  const byteArray = new Uint8Array(byteNumbers)
  return new Blob([byteArray], {type: mimeType})
}

// based on https://gist.github.com/GuilhermeRossato/f9c9a56db090b6d1e247313203fe8209
const getMimeTypeFromExtension = filename => {
  var extension = filename.split('.').pop()
  return (
    {
      aac: 'audio/aac',
      abw: 'application/x-abiword',
      arc: 'application/x-freearc',
      avi: 'video/x-msvideo',
      azw: 'application/vnd.amazon.ebook',
      bin: 'application/octet-stream',
      bmp: 'image/bmp',
      bz: 'application/x-bzip',
      bz2: 'application/x-bzip2',
      cda: 'application/x-cdf',
      csh: 'application/x-csh',
      css: 'text/css',
      csv: 'text/csv',
      doc: 'application/msword',
      docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      eot: 'application/vnd.ms-fontobject',
      epub: 'application/epub+zip',
      gz: 'application/gzip',
      gif: 'image/gif',
      htm: 'text/html',
      html: 'text/html',
      ico: 'image/vnd.microsoft.icon',
      ics: 'text/calendar',
      jar: 'application/java-archive',
      jpeg: 'image/jpeg',
      jpg: 'image/jpeg',
      js: 'text/javascript',
      json: 'application/json',
      jsonld: 'application/ld+json',
      mid: 'audio/midi audio/x-midi',
      midi: 'audio/midi audio/x-midi',
      mjs: 'text/javascript',
      mp3: 'audio/mpeg',
      mp4: 'video/mp4',
      mpeg: 'video/mpeg',
      mpkg: 'application/vnd.apple.installer+xml',
      odp: 'application/vnd.oasis.opendocument.presentation',
      ods: 'application/vnd.oasis.opendocument.spreadsheet',
      odt: 'application/vnd.oasis.opendocument.text',
      oga: 'audio/ogg',
      ogv: 'video/ogg',
      ogx: 'application/ogg',
      opus: 'audio/opus',
      otf: 'font/otf',
      png: 'image/png',
      pdf: 'application/pdf',
      php: 'application/x-httpd-php',
      ppt: 'application/vnd.ms-powerpoint',
      pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      rar: 'application/vnd.rar',
      rtf: 'application/rtf',
      sh: 'application/x-sh',
      svg: 'image/svg+xml',
      swf: 'application/x-shockwave-flash',
      tar: 'application/x-tar',
      tif: 'image/tiff',
      tiff: 'image/tiff',
      ts: 'video/mp2t',
      ttf: 'font/ttf',
      txt: 'text/plain',
      vsd: 'application/vnd.visio',
      wav: 'audio/wav',
      weba: 'audio/webm',
      webm: 'video/webm',
      webp: 'image/webp',
      woff: 'font/woff',
      woff2: 'font/woff2',
      xhtml: 'application/xhtml+xml',
      xls: 'application/vnd.ms-excel',
      xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      xml: 'application/xml',
      xul: 'application/vnd.mozilla.xul+xml',
      zip: 'application/zip',
      '3gp': 'video/3gpp',
      '3g2': 'video/3gpp2',
      '7z': 'application/x-7z-compressed'
    }[extension] || 'application/octet-stream'
  )
}
