Office.onReady()

const transferMailToTocco = async event => {
  const {url, username, apiKey} = getToccoSettings()
  if (!(await areValidSettings(url, username, apiKey))) {
    const pathWithoutFile = window.location.pathname.slice(0, window.location.pathname.lastIndexOf('/'))
    Office.context.ui.displayDialogAsync(
      `${window.location.origin}${pathWithoutFile}/modal.html`,
      {
        height: 50,
        width: 20,
        promptBeforeOpen: false,
        displayInIframe: true
      },
      result => {
        const dialog = result.value
        dialog.addEventHandler(Office.EventType.DialogMessageReceived, args => {
          const {type, url, username, apiKey} = JSON.parse(args.message)
          if (type === 'save') {
            saveToccoSettings(url, username, apiKey, () => {
              dialog.close()
              startImport(event)
            })
          }
        })
        dialog.addEventHandler(Office.EventType.DialogEventReceived, () => {
          event.completed()
        })
      }
    )
    return
  }

  console.log(`Start importing email to tocco url: ${url} username: ${username}`)
  startImport(event)
}

const startImport = event => {
  Office.context.mailbox.item.body.getAsync('html', result => {
    if (result.status === Office.AsyncResultStatus.Failed) {
      console.log(result.error.message)
      return
    }

    processHeaders({body: result.value}, event)
  })
}

const processHeaders = (message, event) => {
  Office.context.mailbox.item.getAllInternetHeadersAsync(result => {
    const headers = result.value
      // some headers uses multiple lines
      .replaceAll('\r\n ', ' ')
      .split('\r\n')
      .map(line => line.trim())
    processAttachments({...message, headers, attachments: []}, 0, event)
  })
}

const processAttachments = (message, attachmentsCounter, event) => {
  const attachments = Office.context.mailbox.item.attachments

  if (attachments.length === attachmentsCounter) {
    doImport(message, event)
    return
  }

  const attachment = attachments[attachmentsCounter]
  Office.context.mailbox.item.getAttachmentContentAsync(attachment.id, async result => {
    switch (result.value.format) {
      case Office.MailboxEnums.AttachmentContentFormat.Base64:
        const uploadUuid = await uploadAttachment(result.value.content, attachment.name)
        if (uploadUuid) {
          const newAttachment = {
            fileName: attachment.name,
            uploadUuid
          }
          processAttachments(
            {
              ...message,
              attachments: [...message.attachments, newAttachment]
            },
            attachmentsCounter + 1,
            event
          )
        } else {
          showNotification(getTranslation('upload.errorAttachment'))
        }
        return
      case Office.MailboxEnums.AttachmentContentFormat.Eml:
        break
      case Office.MailboxEnums.AttachmentContentFormat.ICalendar:
        break
      case Office.MailboxEnums.AttachmentContentFormat.Url:
        break
      default:
    }
    processAttachments(message, attachmentsCounter + 1, event)
  })
}

const doImport = async (messageObj, event) => {
  const message = {
    ...messageObj,
    messageId: Office.context.mailbox.item.internetMessageId,
    from: extractEmailAddress(Office.context.mailbox.item.from),
    to: Office.context.mailbox.item.to.map(address => extractEmailAddress(address)),
    cc: Office.context.mailbox.item.cc.map(address => extractEmailAddress(address)),
    bcc: Office.context.mailbox.item.bcc.map(address => extractEmailAddress(address)),
    subject: Office.context.mailbox.item.subject,
    dateTimeCreated: getLocalISOString(Office.context.mailbox.item.dateTimeCreated)
  }

  try {
    const {url, username, apiKey} = getToccoSettings()
    const response = await fetch(`${url}/nice2/rest/officeintegration/mailarchive`, {
      method: 'PUT',
      headers: {
        Authorization: 'Basic ' + btoa(username + ':' + apiKey),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(message)
    })

    if (response.status === 201) {
      showNotification(getTranslation('upload.successfulImported'))
    } else if (response.status === 200) {
      showNotification(getTranslation('upload.successfulExisting'))
    } else if (response.status === 403) {
      showNotification(getTranslation('upload.missingRole'))
    } else {
      showNotification(getTranslation('upload.error'))
    }

    event.completed()
  } catch (e) {
    console.log(e)
  }
}

const getLocalISOString = date => {
  const offset = date.getTimezoneOffset()
  const offsetAbs = Math.abs(offset)
  const isoString = new Date(date.getTime() - offset * 60 * 1000).toISOString()
  return `${isoString.slice(0, -1)}${offset > 0 ? '-' : '+'}${String(Math.floor(offsetAbs / 60)).padStart(2, '0')}:${String(offsetAbs % 60).padStart(2, '0')}`
}

// You must register the function with the following line.
Office.actions.associate('transferMailToTocco', transferMailToTocco)
