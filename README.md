# tocco-outlook-add-in

## development workflow

- checkout `staging` branch
- change the code and push it
- Gitlab pages (staging url https://outlook-add-in.tocco.ch/staging/) is updated
- test your changes with the staging manifest
- open merge request and clean up your commit history
- after merging the change is automatically deployed for production (url https://outlook-add-in.tocco.ch)

## staging manifest

- create a copy of `manifest.xml`
- set a different `Id`
- change `DisplayName` and all text resources in the `manifest.xml` that you can easier distinguish between production and staging outlook add-in
- replace `https://outlook-add-in.tocco.ch/` with `https://outlook-add-in.tocco.ch/staging/` in `manifest.xml`
- use link below to install staging manifest

## links

- hello world example on which this project is based on https://learn.microsoft.com/en-us/office/dev/add-ins/quickstarts/outlook-quickstart?tabs=yeomangenerator
- Instruction to install add-in https://support.microsoft.com/en-us/office/use-add-ins-in-outlook-1ee261f9-49bf-4ba6-b3e2-2ba7bcab64c8#PickTab=Outlook_on_the_web or quick link for web outlook https://outlook.office.com/mail/inclientstore
